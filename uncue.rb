#!/usr/bin/env ruby

# Copyright (c) 2011, Aldis Berjoza <aldis@bsdroot.lv>

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:

# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require 'optparse'
require 'shellwords'
require 'fileutils'
require 'pathname'

# http://digitalx.org/cue-sheet/syntax/
# http://digitalx.org/cue-sheet/problems/
# http://digitalx.org/cue-sheet/examples/


class InvalidCUEError < RuntimeError
end

class CUE # {{{

  attr_reader :catalog, :cdtextfile
  attr_accessor :genre, :year, :songwriter, :album, :artist, :comment, :discid, :rem

  alias date year
  alias performer artist
  alias title album

  alias date= year=
  alias performer= artist=
  alias title= album=

  def initialize(cue_file_name)
    @cue_source_filename = cue_file_name
    @album = @year = @genre = @artist = @songwriter = @catalog = @cdtextfile = @comment = @discid = nil
    @files = []

    cuefile = File.open(@cue_source_filename, 'r')

    in_head = true
    file = nil
    track = nil

    while ! cuefile.eof? do
      line = cuefile.readline.chomp.strip
      case line
      when /^REM\s+(.+)$/
        rem_data = $1
        case rem_data
        when /^DATE\s+"?(\d+)"?$/;    self.date = $1
        when /^GENRE\s+"?(.+?)"?$/;   self.genre = $1
        when /^COMMENT\s+"?(.*?)"?$/; self.comment = $1
        when /^DISKID\s+"?(.*?)"?$/;  self.discid = $1
        else                          self.rem = $rem_data
        end

      when /^TITLE\s+"?(.+?)"?$/
        if in_head
          self.title = $1
        else
          raise InvalidCUEError, 'TITLE not in TRACK' unless track
          track.title = $1
        end

      when /^CATALOG\s+(.+?)$/
        raise InvalidCUEError, 'CATALOG already defined' unless in_head
        self.catalog = $1

      when /^INDEX\s+(\d{1,2})\s+(.+?)$/
        raise InvalidCUEError, 'INDEX not in TRACK' unless track
        track.add_index!($1.to_i, $2)

      when /^PERFORMER\s+"?(.+?)"?$/
        if in_head
          self.performer = $1
        else
          raise InvalidCUEError, 'PERFORMER not in TRACK' unless track
          track.artist = $1
        end

      when /^SONGWRITER\s+"?(.+?)"?$/
        if in_head
          self.songwriter = $1
        else
          raise InvalidCUEError, 'SONGWRITER not in TRACK' unless track
          track.songwriter = $1
        end

      when /^TRACK\s+(\d{1,2})\s+(\w+)$/
        raise InvalidCUEError, 'Got TRACK but no FILE' unless file
        track = file.add_track!($2, $1.to_i)

      when /^FILE\s+"?(.+?)"?\s+(\w+)$/
        raise InvalidCUEError, 'FILE after FILE witouth TRACKs' if (track == false and file == true)
        file = self.add_file!($1, $2)
        in_head = false
        track = nil

      when /^CDTEXTFILE\s+"?(.+?)"?$/
        raise InvalidCUEError, 'CDTEXTFILE not in head' unless in_head
        self.cdtextfile = $1

      when /^FLAGS\s+(.+?)$/ # not sure about DATA flag
        raise InvalidCUEError, 'FLAGS not in TRACK' unless track
        track.flags = $1.strip.split(/\s+/)

      when /^ISRC\s+(.+?)$/
        raise InvalidCUEError, 'ISRC not in TRACK' unless track
        track.isrc = $1

      when /^POSTGAP\s+(.+?)$/
        raise InvalidCUEError, 'POSTGAP not in TRACK' unless track
        @disc[:postgap] = cue_time_offset_to_ms($1)

      when /^PREGAP\s+(.+?)$/
        raise InvalidCUEError, 'PREGAP not in TRACK' unless track
        @disc[:pregap] = cue_time_offset_to_ms($1)

      end
    end
  end

  def catalog=(code)
    raise ArgumentError, 'Invalid CATALOG code' unless code =~ /^\d{13}$/
    @catalog = code
  end

  def cdtextfile=(filename)
    raise ArgumentError, 'Empty filename' if filename.empty?
    @cdtextfile = filename
  end

  def add_file!(filename, filetype)
    @files.push(CUEFile.new(filename, filetype))
    return @files.last
  end

  def file(fileno=0)
    raise ArgumentError, 'invalid file number' if (fileno < 0 or fileno >= @files.size)
    @files[fileno]
  end

  def to_s
    str = ""
    str += "REM GENRE #{    @genre      }\n"    if @genre
    str += "REM DATE #{     @year       }\n"    if @year
    str += "REM DISCID #{   @discid     }\n"    if @discid
    str += "REM COMMENT #{  @comment    }\n"    if @comment
    str += "REM #{          @rem        }\n"    if @rem
    str += "CATALOG #{      @catalog    }\n"    if @catalog
    str += "PERFORMER \"#{  @artist     }\"\n"  if @artist
    str += "SONGWRITER \"#{ @songwriter }\"\n"  if @songwriter
    str += "CDTEXTFILE \"#{ @cdtextfile }\""    if @cdtextfile
    @files.each { |file| str += file.to_s }
    str
  end
  alias to_str to_s

end # CUE }}}

class CUE::CUEFile # {{{

  attr_reader :type, :name

  def initialize(filename, filetype)
    self.type = filetype
    self.name = filename
    @tracks = []

    @track_offset = nil   # track in array
    @track_number = 0   # track number
  end

  def name=(filename)
    raise ArgumentError, 'empty filename' if filename.empty?
    @name = filename
  end

  def type=(filetype)
    raise ArgumentError, 'invalid filetype' unless filetype =~ /^(BINARY|MOTOROLA|AIFF|WAVE|MP3)$/
    @type = filetype
  end

  def add_track!(type, number=@track_number+1)
    raise ArgumentError, 'invalid track number' if (@track_number > 0 and number != @track_number+1)

    @tracks.push(CUEFileTrack.new(number, type))
    @track_offset = number unless @track_offset
    @track_number = number

    @tracks.last
  end

  def tracks
    @tracks.size
  end

  def track(number=@track_offset)
    track_nr = number - @track_offset
    raise ArgumentErrord, 'invalid track number' if (track_nr < 0 or track_nr >= self.tracks)
    @tracks[track_nr]
  end

  def track2(track_nr)
    raise ArgumentError, 'invalid track number' if (track_nr < 0 or track_nr >= self.tracks)
    @tracks[track_nr]
  end

  def to_s
    str =  "FILE \"#{@name}\" #{@type}\n"
    @tracks.each { |track| str += track.to_s }
    str
  end
  alias to_str to_s

end # class CUEFile }}}

class CUE::CUEFile::CUEFileTrack # {{{

  attr_accessor :artist, :songwriter, :title
  attr_reader :postgap, :pregap, :isrc, :flags, :track, :type
  alias performer artist
  alias performer= artist=

  def initialize(track_nr, track_type)
    raise ArgumentError, 'invalid track type' unless track_type =~ %r#^(AUDIO|CDG|MODE1/(2048|2352)|MODE2/(2336|2352)|CDI/(2336|2352))$#
    raise ArgumentError, 'invalid track number' if (track_nr < 1 or track_nr > 99)

    @track = track_nr
    @type = track_type
    @index = {}

    @title = @artist = @songwriter = @pregap = @postgap = @isrc = @flags = nil
  end

  def index(key=0)
    raise ArgumentError, 'index key out of range' if (key < 0 or key > 99)
    @index[key]
  end

  def postgap=(ms)
    raise ArgumentError, 'negative postgap value' if ms < 0;
    @postgap = ms
  end

  def pregap=(ms)
    raise ArgumentError, 'negative pregap value' if ms < 0;
    @pregap = ms
  end

  def isrc=(code)
    raise ArgumentError, 'invalid isrc code' unless code =~ /^([a-zA-Z0-9]{5}\d{7})$/
    @isrc = code
  end

  def flags=(f)
    raise ArgumentError, 'flags must be array' unless f.is_a? Array
    f.each do |flag|
      raise ArgumentError, "invalid flag a#{f}''" unless flag =~ /^(DCP|4CH|PRE|SCMS|DATA)$/ # not sure about DATA flag
    end
    @flags = f
  end

  def add_index!(key, val)
    raise ArgumentError, 'index key out of range' if (key < 0 or key > 99)
    raise ArgumentError, 'invalid index value' unless val =~ /^\d{2,}:\d\d:([0-6]\d|7[0-5])$/
    @index[key] = val
  end

  def to_s
    str =  "  TRACK #{sprintf "%0.2d", @track} #{@type}\n"

    str += "    TITLE \"#{      @title      }\"\n"  if @title
    str += "    ARTIST \"#{     @artist     }\"\n"  if @artist
    str += "    SONGWRITER \"#{ @songwriter }\"\n"  if @songwriter
    str += "    ISRC #{         @isrc       }\n"    if @isrc
    str += "    PREGAP #{       @pregap     }\n"    if @pregap
    str += "    POSTGAP #{      @postgap    }\n"    if @postgap

    @index.sort{ |a,b| a[1] <=> b[1] }.each do |key, value|
      str += "    INDEX #{sprintf "%0.2d", key} #{value}\n"
    end
    str
  end
  alias to_str to_s

end # class CUEFileTrack }}}


uncue_version = "0.0.5"

class CUE # {{{

  def uncue(template, acodec, ext)
    def escape(str)
      begin
        str.shellescape
      rescue
        str = str.to_s
        retry
      end
    end

    @files.each do |file|

      @cue_source_filename =~ %r#^(/?(.+/)?)#
      path = $1
      test_file = path + file.name
      if File.exist? test_file
        ifilename = test_file
      else
        test_file = @cue_source_filename.sub(/\.cue$/, '')
        raise RuntimeError, "unable to find media file #{file.name}" unless File.exist? test_file
        ifilename = test_file
      end

      file.tracks.times do |i|
        track = file.track2(i)

        case track.type
        when 'AUDIO'
          metadata = "-metadata track=#{escape(track.track)}"
          if track.artist
            metadata += " -metadata artist=#{escape(track.artist)}"
          elsif @artist
            metadata += " -metadata artist=#{escape(@artist)}"
          end
          if track.songwriter
            metadata += " -metadata songwriter=#{escape(track.songwriter)}"
          elsif @songwriter
            metadata += " -metadata songwriter=#{escape(@songwriter)}"
          end
          metadata += " -metadata title=#{  escape(track.title)}" if track.title
          metadata += " -metadata album=#{  escape(@album)     }" if @album
          metadata += " -metadata date=#{   escape(@year)      }" if @year
          metadata += " -metadata genre=#{  escape(@genre)     }" if @genre
          metadata += " -metadata rem=#{    escape(@rem)       }" if @rem
          metadata += " -metadata discid=#{ escape(@discid)    }" if @discid
          metadata += " -metadata comment=#{escape(@comment)   }" if @comment
          metadata += " -metadata catalog=#{escape(@catalog)   }" if @catalog
          metadata += " -metadata isrc=#{   escape(track.isrc) }" if track.isrc

          ss = track.index(0)
          ss = track.index(1) unless ss

          track_range = "-ss #{cue_time_offset_to_ffmpeg_format(ss)}"

          if i != file.tracks-1
            tt = file.track2(i+1).index(0)
            tt = file.track2(i+1).index(1) unless tt
            to = ms_to_ffmpeg_format(cue_time_offset_to_ms(tt) - cue_time_offset_to_ms(ss))
            track_range += " -t #{to}"
          end

          ofilename = format_audio_file_path template, track, ext
          FileUtils.mkdir_p File.dirname(Pathname.new(ofilename))
          system(*Shellwords.split("ffmpeg #{track_range} -i #{ifilename.shellescape} -acodec #{acodec} #{metadata} #{ofilename.shellescape}"))
        else # case track.type
          next # FIXME TODO
        end # case track.type

      end
    end
  end

  private

  def cue_time_offset_to_ffmpeg_format(cue_time_offset)
    raise ArgumentError, 'invalid time offset format' unless cue_time_offset =~ /^\d\d:\d\d:\d\d$/
    (minutes, seconds, frames) = cue_time_offset.split(':')

    minutes = minutes.to_i
    seconds = seconds.to_i
    frames  = frames.to_i

    hours   = (minutes / 60).floor
    minutes = minutes % 60
    xx = (frames * 1.3333333).floor

    sprintf "%0.2d:%0.2d:%0.2d.%0.2d", hours, minutes, seconds, xx
  end

  def cue_time_offset_to_ms(cue_time_offset)
    raise ArgumentError, 'invalid time offset format' unless cue_time_offset =~ /^\d\d:\d\d:\d\d$/
    (minutes, seconds, frames) = cue_time_offset.split(':')

    (minutes.to_i * 6000 + seconds.to_i * 100 + frames.to_i * 1.3333333).floor
  end

  def ms_to_ffmpeg_format(ms)
    xx = ms

    hours = (xx / 360000).floor
    xx = xx % 360000

    minutes = (xx / 6000).floor
    xx = xx % 6000

    seconds = (xx / 100).floor
    xx = xx % 100

    sprintf "%0.2d:%0.2d:%0.2d.%0.2d", hours, minutes, seconds, xx
  end

  def format_audio_file_path(template, track, ext)
    # %album%   - album
    # %artist%  - artist
    # %genre%   - genre
    # %swriter% - song writer
    # %title%   - title
    # %track%   - track
    # %xtrack%  - track with leading zero
    # %year%    - year

    templates = {
      /%artist%/  => track.artist || @artist || 'UNKN_ARTST',
      /%album%/   => @album || 'UNKN_ALBUM',
      /%title%/   => track.title || 'UNKN_TITLE',
      /%genre%/   => @genre || 'UNKN_GENRE',
      /%year%/    => @year || 'UNKN_YEAR',
      /%track%/   => track.track.to_s,
      /%xtrack%/  => "%0.2d" % track.track,
      /%swriter%/ => track.songwriter || @songwriter || 'UNKN_WRITER'
    }

    templates.inject(template + ext) do |result, (patner, replacement)|
      result.gsub(patner, replacement.gsub('/', ' - '))
    end.gsub(/[ ]+/, ' ')
  end

 end # CUE }}}

def guess_ext(acodec)
  case acodec
  when 'flac';        return '.flac'
  when 'libvorbis';   return '.ogg'
  when 'libmp3lame';  return '.mp3'
  when 'libfaac';     return '.aac'
  when 'ac3';         return '.ac3'
  else                raise ArgumentError, 'Unknown extension'
  end
end

def guess_codec(ext)
  case ext
  when /^\.?flac$/i;  return 'flac'
  when /^\.?ogg$/i;   return 'libvorbis'
  when /^\.?mp3$/i;   return 'libmp3lame'
  when /^\.?aac$/i;   return 'ligfaac'
  when /^\.?ac3$/i;   return 'ac3'
  else                raise ArgumentError, 'Unknown codec'
  end
end


options = {}
optparse = OptionParser.new do|opts|
  opts.banner = "Usage: uncue.rb [options] file1.cue file2.cue ..."

  options[:acodec] = nil
  opts.on('--acodec codec', 'Output audio codec (flac|libvorbis|libmp3lame|libfaac|ac3) [Don\'t need when --ext is set]' ) do |codec|
    options[:acodec] = codec
  end

  options[:ext] = nil
  opts.on('--ext extension', 'Output audio extension (.flac|.ogg|.mp3|.aac|.ac3) [Don\'t need when --acodec is set]' ) do |ext|
    options[:ext] = ext
  end

  options[:template] = '%artist%/%album%-%year%/%xtrack%-%title%'
  opts.on('--template template', 'File name template (%artist%, %album%, %title%, %swriter%, %year%, %genre%, %track%, %xtrack%)') do |codec|
    options[:template] = codec
  end

  options[:odir] = nil
  opts.on('--odir path', 'Output directory') do |odir|
    options[:odir] = odir
  end

  opts.on( '-h', '--help', 'Display this screen' ) do
    puts opts
    exit
  end
end
optparse.parse!

if ARGV.count.zero?
  puts optparse
  exit
end

if options[:acodec]
  options[:ext] = guess_ext(options[:acodec])
elsif options[:ext]
  options[:ext] = '.' + options[:ext] unless options[:ext] =~ /^\./
  options[:acodec] = guess_codec(options[:ext])
else
  options[:acodec] = 'flac'
  options[:ext] = guess_ext(options[:acodec])
end


ARGV.each do |cuefile|
  CUE.new(cuefile).uncue(options[:template], options[:acodec], options[:ext])
  #puts CUE.new(cuefile)
end

# vim: set ts=2 sw=2 expandtab:
